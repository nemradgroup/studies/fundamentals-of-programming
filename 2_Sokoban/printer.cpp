#include "printer.h"

#include <stdio.h>
#include <string.h>

#include <SDL.h>

#include "board.h"

#pragma warning(disable:4996)

void DrawString(SDL_Surface *screen, int x, int y, const char *text, SDL_Surface *charset)
{
    int px, py, c;
    SDL_Rect s, d;
    s.w = 8;
    s.h = 8;
    d.w = 8;
    d.h = 8;
    while (*text)
    {
        c = *text & 255;
        px = (c % 16) * 8;
        py = (c / 16) * 8;
        s.x = px;
        s.y = py;
        d.x = x;
        d.y = y;
        SDL_BlitSurface(charset, &s, screen, &d);
        x += 8;
        text++;
    }
}

void DrawSurface(SDL_Surface *screen, SDL_Surface *sprite, int x, int y)
{
    SDL_Rect dest;
    dest.x = x - sprite->w / 2;
    dest.y = y - sprite->h / 2;
    dest.w = sprite->w;
    dest.h = sprite->h;
    SDL_BlitSurface(sprite, NULL, screen, &dest);
}

void DrawPixel(SDL_Surface *surface, int x, int y, Uint32 color)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
    *(Uint32 *)p = color;
}

void DrawLine(SDL_Surface *screen, int x, int y, int l, int dx, int dy, Uint32 color)
{
    for (int i = 0; i < l; i++)
    {
        DrawPixel(screen, x, y, color);
        x += dx;
        y += dy;
    }
}

void DrawRectangle(SDL_Surface *screen, int x, int y, int l, int k, Uint32 outlineColor, Uint32 fillColor)
{
    int i;
    DrawLine(screen, x, y, k, 0, 1, outlineColor);
    DrawLine(screen, x + l - 1, y, k, 0, 1, outlineColor);
    DrawLine(screen, x, y, l, 1, 0, outlineColor);
    DrawLine(screen, x, y + k - 1, l, 1, 0, outlineColor);
    for (i = y + 1; i < y + k - 1; i++)
        DrawLine(screen, x + 1, i, l - 2, 1, 0, fillColor);
}

Printer::Printer(Position *playerPos, Stats *stats) noexcept
    : m_playerPos(playerPos), m_stats(stats)
{
	static bool alreadyLoaded = 0;
	if (alreadyLoaded)
		return;
	else
		alreadyLoaded = 1;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		printf("SDL_Init error: %s\n", SDL_GetError());
		return;
	}

	// tryb pełnoekranowy / fullscreen mode
	//	rc = SDL_CreateWindowAndRenderer(0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP,
	//	                                 &m_window, &m_renderer);
	int rc = SDL_CreateWindowAndRenderer(m_screenWidth, m_screenHeight, 0, &m_window, &m_renderer);
	if (rc != 0)
	{
		SDL_Quit();
		printf("SDL_CreateWindowAndRenderer error: %s\n", SDL_GetError());
		return;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	SDL_RenderSetLogicalSize(m_renderer, m_screenWidth, m_screenHeight);
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);

	SDL_SetWindowTitle(m_window, "Szablon do zdania drugiego 2017");


	m_screen = SDL_CreateRGBSurface(0, m_screenWidth, m_screenHeight, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);

	m_scrtex = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, m_screenWidth, m_screenHeight);


	//SDL_ShowCursor(SDL_DISABLE);

	auto destroySDLThings = [this]()
	{
		SDL_FreeSurface(m_screen);
		SDL_DestroyTexture(m_scrtex);
		SDL_DestroyWindow(m_window);
		SDL_DestroyRenderer(m_renderer);
	};

	m_charset = SDL_LoadBMP("./cs8x8.bmp");
	if (m_charset == NULL)
	{
		printf("SDL_LoadBMP(cs8x8.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}
	SDL_SetColorKey(m_charset, true, 0x000000);

	m_emptyTex = SDL_LoadBMP("./podloga.bmp");
	if (m_emptyTex == NULL)
	{
		printf("SDL_LoadBMP(podloga.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}

	m_chestTex = SDL_LoadBMP("./skrzynka.bmp");
	if (m_emptyTex == NULL)
	{
		printf("SDL_LoadBMP(skrzynka.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}

	m_goalTex = SDL_LoadBMP("./cel.bmp");
	if (m_emptyTex == NULL)
	{
		printf("SDL_LoadBMP(cel.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}

	m_playerTex = SDL_LoadBMP("./postac.bmp");
	if (m_emptyTex == NULL)
	{
		printf("SDL_LoadBMP(postac.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}

	m_wallTex = SDL_LoadBMP("./sciana.bmp");
	if (m_emptyTex == NULL)
	{
		printf("SDL_LoadBMP(sciana.bmp) error: %s\n", SDL_GetError());
		destroySDLThings();
		SDL_Quit();
		return;
	}

	updateFilenames();

	loadLeaderboards();

	for (auto &e : m_leaderboards)
		sortLeaderboardEntries(e.first, 1);
}

Printer::~Printer() noexcept
{
	SDL_FreeSurface(m_charset);
	SDL_FreeSurface(m_screen);
	SDL_DestroyTexture(m_scrtex);
	SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);

	SDL_Quit();
}

void Printer::clear() noexcept
{
    SDL_FillRect(m_screen, NULL, getColor("black"));
}

void Printer::update() noexcept
{
	SDL_UpdateTexture(m_scrtex, NULL, m_screen->pixels, m_screen->pitch);
	//		SDL_RenderClear(m_renderer);
	SDL_RenderCopy(m_renderer, m_scrtex, NULL, NULL);
	SDL_RenderPresent(m_renderer);
}

void Printer::printUpperBanner() noexcept
{
	char text[128];
    DrawRectangle(m_screen, 4, 4, m_screenWidth - 8, 36, getColor("red"), getColor("blue"));
    sprintf(text, "Sokoban, czas trwania = %.1lf s, ilosc ruchow = %d", m_stats->time, m_stats->moves);
    DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 10, text, m_charset);
    sprintf(text, "Esc - wyjscie, strzalki - ruch, n - nowa gra");
    DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 26, text, m_charset);
}

void Printer::printMenu() noexcept
{
	char text[128];
	DrawRectangle(m_screen, 1, 1, m_screenWidth - 2, m_screenHeight - 2, getColor("red"), getColor("blue"));
	sprintf(text, "Witaj w Sokobanie!");
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 10, text, m_charset);
	sprintf(text, "Wybierz ktoras plansze:");
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 26, text, m_charset);

	auto printOption = [this](unsigned index)
	{
		if (index >= m_menuOptions.size())
			return;
		const unsigned x = 250, y = 100;
		
		auto color = m_currentMenuOption == index ? "turquoise" : "dodger blue";
		DrawRectangle(m_screen, x, y + (index - m_firstMenuOptionIndex) * 30, m_screenWidth - 2*x, 25, getColor("red"), getColor(color));
		char text[128];
		sprintf(text, m_menuOptions[index]);
		DrawString(m_screen, x + 20, y + (index - m_firstMenuOptionIndex) * 30 + 10, text, m_charset);
	};

	for (int i = 0; i < 15 && m_firstMenuOptionIndex + i < m_menuOptions.size(); ++i)
		printOption(i + m_firstMenuOptionIndex);

	sprintf(text, "Aby dodac wlasna plansze, nacisnij d");
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 650, text, m_charset);
}

void Printer::printBoard(const Board &board) noexcept
{
	const unsigned texSize = 64;

	const unsigned topMargin = 50;
	const unsigned leftMargin = 50;

	auto drawField = [this, texSize, topMargin, leftMargin](SDL_Surface *texture, int x_, int y_)
	{
		DrawSurface(m_screen, texture, x_*texSize + texSize / 2 + leftMargin, y_*texSize + texSize / 2 + topMargin);
	};

	auto drawAnyField = [this, drawField](FieldContent cont, bool isGoal, int x, int y)
	{
		switch (cont)
		{
		case Empty:
			if (isGoal)
				drawField(m_goalTex, x, y);
			else
				drawField(m_emptyTex, x, y);
			break;
		case Player:
			drawField(m_playerTex, x, y);
			break;
		case Chest:
			drawField(m_chestTex, x, y);
			break;
		case Wall:
			drawField(m_wallTex, x, y);
			break;
		}
	};

	for (int y=0; y<board.height(); ++y)
		for (int x = 0; x < board.width(); ++x)
			drawAnyField(board.getField(x, y)->getValue(), board.getField(x, y)->isGoal(), x, y);

	for (int i = 0; i < board.amountOfMovements(); ++i)
	{
		auto mov = board.getMovement(i);

		if (mov.progress() < 0)
			continue;
		SDL_Surface *tex;
		if (mov.field() == Chest)
			tex = m_chestTex;
		else
			tex = m_playerTex;

		double verProgress = 0, horProgress = 0;
		switch (mov.direction())
		{
		case Up:
			verProgress = -mov.progress();
			break;
		case Down:
			verProgress = mov.progress();
			break;
		case Left:
			horProgress = -mov.progress();
			break;
		case Right:
			horProgress = mov.progress();
			break;
		}
		
		DrawSurface(m_screen, tex, (mov.origin().x + horProgress + 0.5) * texSize + leftMargin, (mov.origin().y + verProgress + 0.5) * texSize + topMargin);
	}
}

void Printer::printWinBanner() noexcept
{
	char text[128];
	DrawRectangle(m_screen, 20, m_screenHeight/2 - 50, m_screenWidth - 40, 100, getColor("yellow"), getColor("green"));
	sprintf(text, "Wygrale(a)s! (r-dodaj do rankingu, enter-wyjdz do menu, n-nowa gra)");
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, m_screenHeight/2 - 25, text, m_charset);
	sprintf(text, "Calkowity czas trwania = %.1lf s, ilosc ruchow = %d", m_stats->time, m_stats->moves);
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, m_screenHeight/2 + 25, text, m_charset);
}

char *Printer::printTextPrompt(int &quit) noexcept
{
	bool done = 0;
	char *text = new char[128];
	sprintf(text, "");

	auto draw = [this, text]()
	{
		DrawRectangle(m_screen, 300, m_screenHeight / 2 - 50, m_screenWidth - 600, 100, getColor("red"), getColor("red"));
		DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, m_screenHeight / 2 - 25, text, m_charset);
		update();
	};

	SDL_StartTextInput();
	bool unnecessaryTriggerKey = 1;
	while (!done)
	{
		SDL_Event event;
		if (SDL_PollEvent(&event))
		{
			if (unnecessaryTriggerKey)
			{
				unnecessaryTriggerKey = 0;
				continue;
			}
			switch (event.type)
			{
			case SDL_QUIT:
				done = 1;
				quit = 1;
				break;
			case SDL_TEXTINPUT:
				strcat(text, event.text.text);
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					sprintf(text, "");
					return text;
				case SDLK_RETURN:
					return text;
				}
			}
		}
		draw();
	}
}

void Printer::printLeaderboards(const char *boardName) noexcept
{
	char text[128];
	DrawRectangle(m_screen, 1, 1, m_screenWidth - 2, m_screenHeight - 2, getColor("red"), getColor("blue"));
	sprintf(text, "Ranking (s-sortowanie, enter-wyjdz do menu)");
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 10, text, m_charset);
	DrawString(m_screen, m_screen->w / 2 - strlen(text) * 8 / 2, 26, boardName, m_charset);

	int boardIndex = -1;
	for (int i=0;i<m_leaderboards.size();++i)
		if (strcmp(m_leaderboards[i].first,boardName) == 0)
		{
			boardIndex = i;
			break;
		}
	if (boardIndex == -1)
		return;

	auto printEntry = [this, boardIndex](unsigned index)
	{
		if (index >= m_leaderboards[boardIndex].second.size())
			return;
		const unsigned x = 250, y = 100;

		auto color = "dodger blue";
		DrawRectangle(m_screen, x, y + (index - m_firstMenuOptionIndex) * 30, m_screenWidth - 2 * x, 25, getColor("red"), getColor(color));
		char text[128];
		sprintf(text, m_leaderboards[boardIndex].second[index].name);
		auto t2 = intToString(m_leaderboards[boardIndex].second[index].stats.moves);
		strcat(text, " Ruchy: ");
		strcat(text, t2);
		strcat(text, " Czas: ");
		t2 = intToString(static_cast<int>(m_leaderboards[boardIndex].second[index].stats.time));
		strcat(text, t2);
		strcat(text, ".");
		t2 = intToString(static_cast<int>(m_leaderboards[boardIndex].second[index].stats.time* 10 ) % 10);
		strcat(text, t2);
		DrawString(m_screen, x + 20, y + (index - m_firstMenuOptionIndex) * 30 + 10, text, m_charset);
	};

	for (int i = 0; i < m_leaderboards[boardIndex].second.size(); ++i)
		printEntry(i);
}

void Printer::updateFilenames() noexcept
{
	m_menuOptions = getFilenames();
	m_currentMenuOption = 0;
	m_firstMenuOptionIndex = 0;
}

void Printer::loadLeaderboards() noexcept
{
	auto file = fopen("leaderboards.txt", "r");
	if (file == nullptr)
		return;

	fseek(file, 0, SEEK_END);
	auto amountOfChars = ftell(file);
	rewind(file);

	auto buffer = new char[amountOfChars];
	if (buffer == nullptr)
		throw "Couldn't allocate that much memory";

	fread(buffer, 1, amountOfChars, file);

	fclose(file);

	for (auto &e : m_leaderboards)
	{
		for (auto &f : e.second)
			delete[] f.name;
		delete e.first;
	}
	m_leaderboards.clear();

	char *playerName;
	unsigned time, moves; // time in deciseconds (1.2s = 12)
	int loadingStage = 0;//board, player, time, moves = 0123
	char str[64] = "";
	int cnt = 0;
	for (int i = 0; i < amountOfChars; ++i)
	{
		if (loadingStage == 0)
		{
			if (buffer[i] == '{')
			{
				char *bn = new char[64];
				strcpy(bn, str);
				strcpy(str, "");
				m_leaderboards.push_back({ bn, {} });
				++loadingStage;
				++i; // \n
			}
			else
			{
				char s[2];
				s[0] = buffer[i];
				s[1] = 0;
				strcat(str, s);
			}
		}
		else
		{
			if (buffer[i] == '\n')
			{
				moves = stringToInt(str, strlen(str));
				strcpy(str, "");
				m_leaderboards[m_leaderboards.size()-1].second.push_back({ {static_cast<double>(time) / 10, moves}, playerName });
				loadingStage = 1;
			}
			else if (buffer[i] == ';')
			{
				if (loadingStage == 1)
				{
					playerName = new char[64];
					strcpy(playerName, str);
				}
				else // loading stage == 2
					time = stringToInt(str, strlen(str));
				strcpy(str, "");
				++loadingStage;
			}
			else if (buffer[i] == '}')
			{
				loadingStage = 0;
				++i; // \n
			}
			else
			{
				char s[2];
				s[0] = buffer[i];
				s[1] = 0;
				strcat(str, s);
			}
		}

		/*
		
		example

		plansza1.txt{
		gracz 1;10;45
		gracz2;5;30
		}
		plansza2.txt{
		gracz3;10;20
		}
		
		*/
	}

	delete[] buffer;
}

void Printer::saveLeaderboards() noexcept
{
	auto file = fopen("leaderboards.txt", "w");
	if (file == nullptr)
		return;
	for (const auto &e : m_leaderboards)
	{
		fwrite(e.first, sizeof(char), strlen(e.first), file);
		fwrite("{\n", sizeof(char), 2, file);
		for (const auto &f : e.second)
		{
			fwrite(f.name, sizeof(char), strlen(f.name), file);
			fwrite(";", sizeof(char), 1, file);
			auto s = intToString(f.stats.time*10);
			fwrite(s, sizeof(char), strlen(s), file);
			fwrite(";", sizeof(char), 1, file);
			s = intToString(f.stats.moves);
			fwrite(s, sizeof(char), strlen(s), file);
			fwrite("\n", sizeof(char), 1, file);
		}
		fwrite("}", sizeof(char), 1, file);
	}

	fclose(file);
}

void Printer::addLeaderboardEntry(char *boardName, char *playerName, double time, unsigned moves)
{
	int boardIndex=-1;
	for (int i=0;i<m_leaderboards.size();++i)
		if (strcmp(m_leaderboards[i].first, boardName) == 0)
		{
			boardIndex = i;
			break;
		}
	if (boardIndex == -1)
	{
		m_leaderboards.push_back({ boardName, {} });
		boardIndex = m_leaderboards.size() - 1;
	}

	m_leaderboards[boardIndex].second.push_back({ {time, moves}, playerName });
	sortLeaderboardEntries(boardName, 1);
}

void Printer::sortLeaderboardEntries(const char *boardName, bool byTime) noexcept
{
	int boardIndex = -1;
	for (int i = 0; i<m_leaderboards.size(); ++i)
		if (strcmp(m_leaderboards[i].first, boardName) == 0)
		{
			boardIndex = i;
			break;
		}
	if (boardIndex == -1)
		return;

	int j;

	LeaderboardEntry t;

	for (int i = 0; i < m_leaderboards[boardIndex].second.size(); i++) {
		j = i;

		while (j > 0 && (byTime ?
			(m_leaderboards[boardIndex].second[j].stats.time < m_leaderboards[boardIndex].second[j - 1].stats.time) :
			(m_leaderboards[boardIndex].second[j].stats.moves < m_leaderboards[boardIndex].second[j - 1].stats.moves)))
		{
			t = m_leaderboards[boardIndex].second[j];
			m_leaderboards[boardIndex].second[j] = m_leaderboards[boardIndex].second[j - 1];
			m_leaderboards[boardIndex].second[j - 1] = t;
			j--;
		}
	}
}

void Printer::goUpInMenu() noexcept
{
	if (m_currentMenuOption > 0)
		--m_currentMenuOption;
	if (m_currentMenuOption < m_firstMenuOptionIndex)
		--m_firstMenuOptionIndex;
}

void Printer::goDownInMenu() noexcept
{
	if (m_currentMenuOption < m_menuOptions.size() - 1)
		++m_currentMenuOption;
	if (m_currentMenuOption >= m_firstMenuOptionIndex + 15)
		++m_firstMenuOptionIndex;
}

const char * Printer::currentMenuOption() const noexcept
{
	if (m_menuOptions.isEmpty())
		return nullptr;
	return m_menuOptions[m_currentMenuOption];
}

int Printer::getColor(const char *color) const noexcept
{
	auto col = [this](int r, int g, int b)
	{
		return SDL_MapRGB(m_screen->format, r, g, b);
	};

	if (!strcmp(color, "black"))
		return col(0x00, 0x00, 0x00);
	if (!strcmp(color, "green"))
		return col(0x00, 0xff, 0x00);
	if (!strcmp(color, "red"))
		return col(0xff, 0x00, 0x00);
	if (!strcmp(color, "blue"))
		return col(0x00, 0x00, 0xff);
	if (!strcmp(color, "yellow"))
		return col(0xff, 0xf8, 0x33);
	if (!strcmp(color, "lightgrey"))
		return col(0x77, 0x77, 0x77);
	if (!strcmp(color, "silver"))
		return col(0xdd, 0xdd, 0xdd);
	if (!strcmp(color, "dodger blue"))
		return col(0x1e, 0x90, 0xff);
	if (!strcmp(color, "turquoise"))
		return col(0x00, 0xf5, 0xff);
	return col(0xff, 0xff, 0xff);
}

Vector<char*> getFilenames()
{
	auto file = fopen("list.txt", "r");
	if (file == nullptr)
	{
		return {};
	}

	fseek(file, 0, SEEK_END);
	auto amountOfChars = ftell(file);
	rewind(file);

	auto buffer = new char[amountOfChars];
	if (buffer == nullptr)
		throw "Couldn't allocate that much memory";

	fread(buffer, 1, amountOfChars, file);

	fclose(file);

	Vector <char *> r;

	int cnt = 0, endlCnt = 0;
	char name[256];
	char *singleFilename;
	for (int i = 0; i < amountOfChars; ++i)
	{
		if (buffer[i] == '\n')
		{
			++endlCnt;
			if (cnt == 0)
				continue;

			singleFilename = new char[cnt + 1];
			for (int j = 0; j < cnt; ++j)
				singleFilename[j] = name[j];
			singleFilename[cnt] = 0;

			r += singleFilename;
			cnt = 0;
		}
		else
			name[cnt++] = buffer[i];
	}

	if (cnt != 0 && cnt - endlCnt != 0)
	{
		cnt -= endlCnt;
		singleFilename = new char[cnt + 1];
		for (int j = 0; j < cnt; ++j)
			singleFilename[j] = name[j];
		singleFilename[cnt] = 0;

		r += singleFilename;
	}

	delete[] buffer;

	return r;
}

bool canReadFile(const char *path) noexcept
{
	auto file = fopen(path, "r");
	if (file == nullptr)
		return 0;

	fclose(file);

	return 1;
}

void addFilename(const char *filename)
{
	auto file = fopen("list.txt", "a");
	if (file == nullptr)
		throw "Error opening list file";
	fwrite("\n", sizeof(char), sizeof(char), file);
	fwrite(filename, sizeof(char), strlen(filename), file);
	fclose(file);
}
