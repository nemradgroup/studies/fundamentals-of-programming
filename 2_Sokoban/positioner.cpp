#include "positioner.h"

#include "board.h"
#include "helpers.h"

Positioner::Positioner(Board *board) noexcept
    : m_board(board) {}

bool Positioner::move(Direction direction) noexcept
{
    Position curPos = m_board->playerPos();

    auto get = [this](unsigned x, unsigned y)
    {
        return m_board->getField(x, y)->getValue();
    };
    auto set = [this](unsigned x, unsigned y, FieldContent value)
    {
        m_board->accessField(x, y)->setValue(value);
    };

    if (direction == Up)
    {
        if (curPos.y) // not at the top
        {
			if (get(curPos.x, curPos.y - 1) == Empty) // can move normally
			{
				createMovement(1, curPos, Up);
				set(curPos.x, curPos.y, Empty);
				return 1;
			}
			else if (get(curPos.x, curPos.y - 1) == Chest && curPos.y >= 2 && get(curPos.x, curPos.y - 2) == Empty) // can move and push
			{
				createMovement(0, { curPos.x, curPos.y - 1 }, Up);
				createMovement(1, curPos, Up);
				set(curPos.x, curPos.y, Empty);
				return 1;
			}
			else
				return 0;
        }
    }
    else if (direction == Down)
    {
        if (curPos.y < m_board->height() - 1)
        {
            if (get(curPos.x, curPos.y + 1) == Empty) // can move normally
            {
				createMovement(1, curPos, Down);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
            else if (get(curPos.x, curPos.y + 1) == Chest && curPos.y < m_board->height() - 2 && get(curPos.x, curPos.y + 2) == Empty) // can move and push
            {
				createMovement(0, { curPos.x, curPos.y + 1 }, Down);
				createMovement(1, curPos, Down);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
			else
				return 0;
        }
    }
    else if (direction == Left)
    {
        if (curPos.x)
        {
            if (get(curPos.x - 1, curPos.y) == Empty) // can move normally
            {
				createMovement(1, curPos, Left);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
            else if (get(curPos.x - 1, curPos.y) == Chest && curPos.x >= 2 && get(curPos.x - 2, curPos.y) == Empty) // can move and push
            {
				createMovement(0, { curPos.x - 1, curPos.y }, Left);
				createMovement(1, curPos, Left);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
			else
				return 0;
        }
    }
    else
    {
        if (curPos.x < m_board->width() - 1)
        {
            if (get(curPos.x + 1, curPos.y) == Empty) // can move normally
            {
				createMovement(1, curPos, Right);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
            else if (get(curPos.x + 1, curPos.y) == Chest && curPos.x < m_board->width() - 2 && get(curPos.x + 2, curPos.y) == Empty) // can move and push
            {
				createMovement(0, { curPos.x + 1, curPos.y }, Right);
				createMovement(1, curPos, Right);
                set(curPos.x, curPos.y, Empty);
				return 1;
            }
			else
				return 0;
        }
    }
}

void Positioner::createMovement(bool player, const Position &origin, Direction direction) noexcept
{
	m_board->addMovement(MovementInfo{ player ? Player : Chest, origin, direction });
}
