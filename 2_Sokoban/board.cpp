#include "board.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "stl/pair.h"

#pragma warning(disable:4996)

MovementInfo::MovementInfo() noexcept
	: m_direction(Up), m_field(Null), m_origin{ -1,-1 }, m_progress(0) {}

MovementInfo::MovementInfo(FieldContent field, const Position &origin, Direction direction) noexcept
	: m_field(field), m_origin(origin), m_direction(direction), m_progress(0) {}

void MovementInfo::pushProgress(double delta) noexcept
{
	m_progress += delta;
}

Board::Board() noexcept
{
	/*m_currentPathToFile = "10x10.txt";
    resize(10,10);
    reset();*/
}

FieldBase *Board::accessField(unsigned x, unsigned y) noexcept
{
    if (x<width() && y<height())
        return m_fields[x][y].get();
    return nullptr;
}

const FieldBase *Board::getField(unsigned x, unsigned y) const noexcept
{
    if (x<width() && y<height())
        return m_fields[x][y].get();
    return nullptr;
}

void Board::setField(unsigned x, unsigned y, FieldBase *field) noexcept
{
    if (x<width() && y<height())
        m_fields[x][y] = field;
}

void Board::addMovement(const MovementInfo &mov) noexcept
{
	m_activeMovements += mov;
}

MovementInfo Board::getMovement(unsigned index) const noexcept
{
	return m_activeMovements.value(index);
}

unsigned Board::amountOfMovements() const noexcept
{
	return m_activeMovements.size();
}

bool Board::continueMovements(double progressDelta) noexcept // returns true if anything completed
{
	for (auto &e : m_activeMovements)
		e.pushProgress(progressDelta);
	return checkForCompletedMovements();
}

bool Board::checkForCompletedMovements() noexcept // returns true if anything completed
{
	bool compl = 0;
	for (int i=0;i<m_activeMovements.size();++i)
		if (m_activeMovements[i].isCompleted())
		{
			accessField(m_activeMovements[i].target().x, m_activeMovements[i].target().y)->setValue(m_activeMovements[i].field());
			m_activeMovements.remove(i--);
			compl = 1;
		}
	return compl;
}

void Board::clearMovements() noexcept
{
	m_activeMovements.clear();
}

Position Board::playerPos() const noexcept
{
    for (int i=0; i<width(); ++i)
        for (int j=0; j<height(); ++j)
            if (m_fields[i][j].get()->getValue() == FieldContent::Player)
                return {i,j};
    return {-1,-1};
}

void Board::clear() noexcept
{
    int w = width(), h = height();
    for (int y=0; y<h; ++y)
        for (int x=0; x<w; ++x)
            m_fields[x][y] = {new Field<false>};

	clearMovements();
}

void Board::reset() noexcept
{
	clear();
    loadFromFile(m_currentPathToFile);
}

unsigned Board::width() const noexcept
{
    return m_fields.size();
}

unsigned Board::height() const noexcept
{
    return m_fields.isEmpty() ? 0 : m_fields[0].size();
}

typedef bool goal;
typedef Pair <FieldContent, goal> FieldData;

FieldData loadedCharToFieldData(char c) noexcept
{
    switch (c)
    {
    case '.':
        return {FieldContent::Empty, 0};
    case '#':
        return {FieldContent::Wall, 0};
    case 'X':
        return {FieldContent::Empty, 1};
    case 'O':
        return {FieldContent::Chest, 1};
    case 'o':
        return {FieldContent::Chest, 0};
    case 'P':
        return {FieldContent::Player, 1};
    case 'p':
        return {FieldContent::Player, 0};
	default:
		return {FieldContent::Null, 0};
    }
}

char fieldDataToSavedChar(FieldData d) noexcept
{
    switch (d.first)
    {
    case FieldContent::Null:
        return ' ';
    case FieldContent::Empty:
        if (d.first)
            return 'X';
        else
            return '.';
    case FieldContent::Wall:
        return '#';
    case FieldContent::Chest:
        if (d.first)
            return 'O';
        else
            return 'o';
    case FieldContent::Player:
        if (d.first)
            return 'P';
        else
            return 'p';
    }
}

void Board::loadFromFile(const char *name)
{
    auto file = fopen (name , "r");
    if (file == nullptr)
    {
        return;
    }

    fseek(file , 0, SEEK_END);
    auto amountOfChars = ftell(file);
    rewind(file);

    auto buffer = new char[amountOfChars];
    if (buffer == nullptr)
        throw "Couldn't allocate that much memory";

	fread(buffer, 1, amountOfChars, file);

    fclose(file);

	int width = 0, height = 0;
	char buf[10];
	int bufCnt = 0;
	int xLength = 0, yLength = 0;
	for (int i = 0; i < amountOfChars; ++i)
	{
		if (buffer[i] == 'x')
		{
			xLength = bufCnt;
			bufCnt = 0;
			char *str = new char[xLength];
			for (int j = 0; j < xLength; ++j)
				str[j] = buf[j];
			width = stringToInt(str, xLength);
			delete[] str;
		}
		else if (buffer[i] == '\n')
		{
			yLength = bufCnt;
			char *str = new char[yLength];
			for (int j = 0; j < yLength; ++j)
				str[j] = buf[j];
			height = stringToInt(str, yLength);
			delete[] str;
			break;
		}
		else
			buf[bufCnt++] = buffer[i];
	}

	int pos0 = xLength + 1 + yLength + 1;

    m_fields.resize(width);
    for (int i=0; i<width; ++i)
        m_fields[i].resize(height);

    for (int y=0; y<height; ++y)
        for (int x=0; x<width; ++x)
        {
            auto fv = loadedCharToFieldData(buffer[pos0 + y*(width+1) + x]);
            if (fv.second)
                m_fields[x][y] = {new Field<true>{fv.first}};
            else
                m_fields[x][y] = {new Field<false>{fv.first}};
        }

    delete buffer;

	m_currentPathToFile = name;
}

bool Board::isCompleted() const noexcept
{
	for (int i = 0; i < height(); ++i)
		for (int j = 0; j < width(); ++j)
			if (m_fields[j][i].get()->isGoal() && m_fields[j][i].get()->getValue() != Chest)
				return 0;
	return 1;
}
