#pragma once

#include "stl/pair.h"
#include "stl/vector.h"
#include "stl/uniqueptr.h"

#include "field.h"
#include "helpers.h"

class MovementInfo
{
public:
	MovementInfo() noexcept;
	explicit MovementInfo(FieldContent field, const Position &origin, Direction direction) noexcept;

	inline FieldContent field() const noexcept
	{
		return m_field;
	}
	inline Position origin() const noexcept
	{
		return m_origin;
	}
	inline Direction direction() const noexcept
	{
		return m_direction;
	}
	inline double progress() const noexcept
	{
		return m_progress;
	}
	Position target() const noexcept
	{
		switch (m_direction)
		{
		case Up:
			return { m_origin.x, m_origin.y - 1 };
		case Down:
			return { m_origin.x, m_origin.y + 1 };
		case Left:
			return { m_origin.x - 1, m_origin.y };
		case Right:
			return { m_origin.x + 1, m_origin.y };
		}
	}
	inline bool isCompleted() const noexcept
	{
		return progress() >= 1;
	}

	void pushProgress(double delta) noexcept;

private:
	FieldContent m_field;
	Position m_origin;
	Direction m_direction;
	double m_progress;
};

typedef Vector <Vector <UniquePtr <FieldBase> > > Fields;

class Board
{
public:
    Board() noexcept;

    FieldBase *accessField(unsigned x, unsigned y) noexcept;
    const FieldBase *getField(unsigned x, unsigned y) const noexcept;
    void setField(unsigned x, unsigned y, FieldBase *field) noexcept;

	void addMovement(const MovementInfo &mov) noexcept;
	MovementInfo getMovement(unsigned index) const noexcept;
	unsigned amountOfMovements() const noexcept;

	bool continueMovements(double progressDelta) noexcept;
	bool checkForCompletedMovements() noexcept;
	void clearMovements() noexcept;

    Position playerPos() const noexcept;

    void clear() noexcept; // just clears
    void reset() noexcept; // clears and reloads default

    unsigned width() const noexcept;
    unsigned height() const noexcept;

    void loadFromFile(const char *name);

	bool isCompleted() const noexcept;

private:
	const char *m_currentPathToFile;
    Fields m_fields;
	Vector <MovementInfo> m_activeMovements;
};
