#include <string.h>
#include <stdio.h>

#include <SDL.h>

#include "board.h"
#include "positioner.h"
#include "printer.h"

int main(int argc, char **argv)
{
	int t1, t2, quit;
	double delta;

	int x, y, xVicinity, yVicinity;

	Position playerPos;

	Stats currentStats;
	Board board;
	Printer printer{&playerPos, &currentStats};
	Positioner positioner{&board};
	
	bool boardCompleted = 0;
	
	bool canMove = 1;

	bool isGameOn = 0;

	bool leaderboardsOn = 0;
	bool sortLeaderboardsByTime = 1;

	SDL_Event event;
	
	t1 = SDL_GetTicks();

	quit = 0;

    while (!quit)
    {
		if (!isGameOn)
		{
			printer.clear();
			if (!leaderboardsOn)
				printer.printMenu();
			else
				printer.printLeaderboards(printer.currentMenuOption());
			printer.update();

			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_KEYDOWN:
					if (!leaderboardsOn)
					{
						if (event.key.keysym.sym == SDLK_ESCAPE)
							quit = 1;
						else if (event.key.keysym.sym == SDLK_UP)
							printer.goUpInMenu();
						else if (event.key.keysym.sym == SDLK_DOWN)
							printer.goDownInMenu();
						else if (event.key.keysym.sym == SDLK_RETURN)
						{
							currentStats.time = 0;
							currentStats.moves = 0;
							t1 = SDL_GetTicks();
							boardCompleted = 0;
					
							board.loadFromFile(printer.currentMenuOption());

							isGameOn = 1;
						}
						else if (event.key.keysym.sym == SDLK_d)
						{
							auto name = printer.printTextPrompt(quit);
							if (name != "" && canReadFile(name))
							{
								addFilename(name);
								printer.updateFilenames();
							}
						}
						else if (event.key.keysym.sym == SDLK_r)
							leaderboardsOn = 1;
					}
					else
					{
						if (event.key.keysym.sym == SDLK_ESCAPE)
							quit = 1;
						else if (event.key.keysym.sym == SDLK_RETURN)
							leaderboardsOn = 0;
						else if (event.key.keysym.sym == SDLK_s)
						{
							sortLeaderboardsByTime = !sortLeaderboardsByTime;
							printer.sortLeaderboardEntries(printer.currentMenuOption(), sortLeaderboardsByTime);
						}
					}
					break;
				case SDL_QUIT:
					quit = 1;
					break;
				}
			}
		}
		else
		{
			playerPos = board.playerPos();
			if (!boardCompleted)
			{
				t2 = SDL_GetTicks();

				delta = (t2 - t1) * 0.001;
				t1 = t2;
			
				currentStats.time += delta;
				const double movementTime = 0.25; // in s
				if (board.continueMovements(delta / movementTime))
					boardCompleted = board.isCompleted();
			}
	
			canMove = (!boardCompleted) && board.amountOfMovements() == 0;
	
			printer.clear();
			printer.printBoard(board);
			printer.printUpperBanner();
			if (boardCompleted)
				printer.printWinBanner();
			printer.update();
	
		    while (SDL_PollEvent(&event))
			{
				switch (event.type)
		        {
					case SDL_KEYDOWN:
						if (event.key.keysym.sym == SDLK_ESCAPE)
							quit = 1;
						else if (event.key.keysym.sym == SDLK_n)
						{
							boardCompleted = 0;
							currentStats.time = 0;
							currentStats.moves = 0;
							board.reset();
						}
						else if (event.key.keysym.sym == SDLK_UP)
						{
							if (canMove)
							{
								currentStats.moves += positioner.move(Up);
								canMove = 0;
							}
						}
						else if (event.key.keysym.sym == SDLK_DOWN)
						{
							if (canMove)
							{
								currentStats.moves += positioner.move(Down);
								canMove = 0;
							}
						}
						else if (event.key.keysym.sym == SDLK_LEFT)
						{
							if (canMove)
							{
								currentStats.moves += positioner.move(Left);
								canMove = 0;
							}
						}
						else if (event.key.keysym.sym == SDLK_RIGHT)
						{
							if (canMove)
							{
								currentStats.moves += positioner.move(Right);
								canMove = 0;
							}
						}
						else if (event.key.keysym.sym == SDLK_RETURN)
						{
							board.clear();
							isGameOn = 0;
						}
						else if (event.key.keysym.sym == SDLK_r)
						{
							if (boardCompleted)
							{
								char *bn = new char[64];
								strcpy(bn, printer.currentMenuOption());
								printer.addLeaderboardEntry(bn, printer.printTextPrompt(quit), currentStats.time, currentStats.moves);
								board.clear();
								isGameOn = 0;
							}
						}
						break;
					case SDL_MOUSEBUTTONDOWN:
						x = (event.button.x - 50) / 64; printf("%d", x);
						if (x < 1 || x>=board.width()-1)
							break;
						y = (event.button.y - 50) / 64; printf("%d", y);
						if (y < 1 || y >= board.height()-1)
							break;
						xVicinity = x > playerPos.x ? x - playerPos.x == 1 : playerPos.x - x == 1;
						yVicinity = y > playerPos.y ? y - playerPos.y == 1 : playerPos.y - y == 1;
						if ((xVicinity || yVicinity) && !(xVicinity && yVicinity))
						{
							if (xVicinity && playerPos.y == y)
							{
								if (x > playerPos.x)
									currentStats.moves+=positioner.move(Right);
								else
									currentStats.moves += positioner.move(Left);
							}
							else if (playerPos.x == x)
							{
								if (y > playerPos.y)
									currentStats.moves += positioner.move(Down);
								else
									currentStats.moves += positioner.move(Up);
							}
						}
						else
						{

						}
						break;
					case SDL_QUIT:
						quit = 1;
						break;
			    }
			}
		}
    }
	printer.saveLeaderboards();
	return 0;
}
