#pragma once

#include <SDL.h>

#include "helpers.h"
#include "stl/vector.h"
#include "stl/pair.h"

struct Stats
{
	double time;
	unsigned moves;
};

struct LeaderboardEntry
{
	Stats stats;
	char *name;
};

class Board;

class Printer
{
public:
    Printer(Position *posOfCursor, Stats *stats) noexcept;
	~Printer() noexcept;

    void clear() noexcept;
	void update() noexcept;

    void printUpperBanner() noexcept;
    void printMenu() noexcept;
    void printBoard(const Board &board) noexcept;
    void printWinBanner() noexcept;
	char *printTextPrompt(int &quit) noexcept;
	void printLeaderboards(const char *boardName) noexcept;

	void updateFilenames() noexcept;
	void loadLeaderboards() noexcept;
	void saveLeaderboards() noexcept;
	void addLeaderboardEntry(char *boardName, char *playerName, double time, unsigned moves);
	void sortLeaderboardEntries(const char *boardName, bool byTime/*or moves*/) noexcept;
	void goUpInMenu() noexcept;
	void goDownInMenu() noexcept;
	const char *currentMenuOption() const noexcept;

private:
	int getColor(const char *color) const noexcept;

	Position *m_playerPos;
	Stats *m_stats;

	const unsigned m_screenWidth = 1000;
	const unsigned m_screenHeight = 800;

	SDL_Surface *m_screen, *m_charset;
	SDL_Surface *m_emptyTex, *m_playerTex, *m_wallTex, *m_goalTex, *m_chestTex;
	SDL_Texture *m_scrtex;
	SDL_Window *m_window;
	SDL_Renderer *m_renderer;

	Vector <char *> m_menuOptions;
	unsigned m_currentMenuOption = 0;
	unsigned m_firstMenuOptionIndex = 0;

	Vector <Pair <char *, Vector<LeaderboardEntry> > > m_leaderboards;
};

Vector <char *> getFilenames();
void addFilename(const char *filename);
bool canReadFile(const char *path) noexcept;

void DrawString(SDL_Surface *screen, int x, int y, const char *text, SDL_Surface *charset);
void DrawSurface(SDL_Surface *screen, SDL_Surface *sprite, int x, int y);
void DrawPixel(SDL_Surface *surface, int x, int y, Uint32 color);
void DrawLine(SDL_Surface *screen, int x, int y, int l, int dx, int dy, Uint32 color);
void DrawRectangle(SDL_Surface *screen, int x, int y, int l, int k, Uint32 outlineColor, Uint32 fillColor);