#pragma once

template <class T>
class Array
{
public:
    Array() noexcept
        : m_data(nullptr), m_size(0) {}
    explicit Array(const T *data, unsigned length) noexcept
    {
        m_data=new T[length];
        m_size=length;
        for (int i=0;i<length;++i)
            m_data[i] = data[i];
    }
    Array(const Array<T> &other) noexcept
    {
        m_size = other.size();
        if (m_size == 0)
        {
            m_data = nullptr;
            return;
        }
        m_data = new T[m_size];
        for(int i=0;i<m_size;++i)
            m_data[i] = other.m_data[i];
    }
    explicit Array(Array<T> &&other) noexcept
        : m_data(other.m_data), m_size(other.m_size)
    {
        other.m_data = nullptr;
        other.m_size = 0;
    }
    virtual ~Array() noexcept
    {
        if (m_data != nullptr)
            delete[] m_data;
    }

    const T &operator[](unsigned index) const noexcept
    {
        return m_data[index];
    }
    T &operator[](unsigned index) noexcept
    {
        return m_data[index];
    }

    const T &at(unsigned index) const noexcept
    {
        return (*this)[index];
    }

    T value(unsigned index, const T &defaultValue = {}) const
    {
        if (m_data == nullptr)
        {
            throw "No data available!";
            return {};
        }
        if (index < m_size)
            return m_data[index];
        return defaultValue;
    }

    Array operator+(const Array<T> &other) const noexcept
    {
        int ts=size();
        int os=other.size();
        T *t = new T[ts + os];
        for (int i=0;i<ts;++i)
            t[i] = m_data[i];
        for (int i=0;i<os;++i)
            t[i+ts] = other.m_data[i];
        return Array<T>{t, ts+os};
    }
    Array &operator+=(const Array<T> &other) noexcept
    {
        *this = *this + other;
        return *this;
    }

    Array &operator=(const Array<T> &other) noexcept
    {
        if (this == &other)
            return *this;

        if (m_data != nullptr)
            delete[] m_data;
        if (other.size() == 0)
        {
            m_data=nullptr;
			m_size = 0;
            return *this;
        }
        int s=other.size();
        m_data = new T[s];
        m_size = s;
        for (int i=0;i<s;++i)
            m_data[i] = other[i];
        return *this;
    }

    bool operator==(const Array<T> &other) const noexcept
    {
        if (size() != other.size())
            return 0;
        int s=size();
        for (int i=0;i<s;++i)
            if (m_data[i] != other[i])
                return 0;
        return 1;
    }
    inline bool operator!=(const Array<T> &other) const noexcept
    {
        return !(*this==other);
    }

    unsigned size() const noexcept
    {
        return m_size;
    }

    bool isEmpty() const noexcept
    {
        return m_data == nullptr;
    }

private:
    T *m_data;
    unsigned m_size;
};
