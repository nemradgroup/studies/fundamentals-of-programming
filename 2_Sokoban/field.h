#pragma once

enum FieldContent
{
    Null,
    Empty,
    Player,
    Chest,
    Wall
};

class FieldBase
{
public:
    FieldBase() noexcept
        : m_value(FieldContent::Null) {}
    explicit FieldBase(FieldContent value) noexcept
        : m_value(value) {}
    virtual ~FieldBase() noexcept = default;

    inline FieldContent getValue() const noexcept
    {
        return m_value;
    }

    inline void setValue(FieldContent value) noexcept
    {
        m_value = value;
    }

    inline virtual bool isGoal() const noexcept
    {
        return 0;
    }

protected:
    FieldContent m_value;
};

template <bool goal>
class Field : public FieldBase
{
public:
    Field() noexcept = default;
    explicit Field(FieldContent value) noexcept
        : FieldBase(value) {}

    inline bool isGoal() const noexcept final
    {
        return goal;
    }
};
