#pragma once

#include "stl/vector.h"

#include "helpers.h"

class Board;

class Positioner
{
public:
    Positioner(Board *board) noexcept;

    bool move(Direction direction) noexcept; // returns true if moved
private:
	void createMovement(bool player/*or chest*/, const Position &origin, Direction direction) noexcept;
	
    Board *m_board;
};
