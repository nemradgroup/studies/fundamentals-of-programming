#pragma once

#include "helpers.h"
#include "stl/vector.h"

class Board;
class Judge;

class ConsolePrinter
{
public:
    ConsolePrinter(Position *posOfCursor, KeyInfo *keyInfo) noexcept;

    void clear() noexcept;

    void printMenu() noexcept;
    void printBoard(const Board &board) noexcept;
    void printCursor() noexcept;
    void printTips(const Vector<char *> &tips) noexcept;
    void printSizePrompt(Board *board) noexcept;
    void markAssholeFields(Board &board, Judge &judge) noexcept;
    void printHVLabels(Board &board) noexcept;
    void printAutomodeInfo() noexcept;
    void printAutomodeWarning() noexcept;

    void moveCursor(int x, int y, unsigned sizeOfBoard) noexcept;

    void resetCursor() noexcept;

private:
    Position *m_posOfCursor;
    KeyInfo *m_keyInfo;
    const Position m_posOfMenu = {5,2};
    const Position m_posOfBoard = {65,5};
};
