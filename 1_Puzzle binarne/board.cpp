#include "board.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "helpers.h"
#include "judge.h"

#pragma warning(disable:4996)

Board::Board() noexcept
{
    resize(12);
    reset();
}

FieldBase *Board::accessField(unsigned x, unsigned y) noexcept
{
    if (x<m_fields.size() && y<m_fields.size())
        return m_fields[x][y].get();
    return nullptr;
}

const FieldBase *Board::getField(unsigned x, unsigned y) const noexcept
{
    if (x<m_fields.size() && y<m_fields.size())
        return m_fields[x][y].get();
    return nullptr;
}

void Board::setField(unsigned x, unsigned y, FieldBase *field) noexcept
{
    if (x<m_fields.size() && y<m_fields.size())
        m_fields[x][y] = field;
}

void Board::clear() noexcept
{
    int size = m_fields.size();
    for (int y=0; y<size; ++y)
        for (int x=0; x<size; ++x)
            m_fields[x][y] = {new Field<true>};
}

void Board::reset() noexcept
{
    clear();
    char path[30];
    char *n = intToString(size());
    strcpy(path, n);
    strcat(path, "x");
    strcat(path, n);
    strcat(path, ".txt");
    delete[] n;
    loadFromFile(path);
}

void Board::randomize() noexcept
{
    clear();
    int n = m_fields.size();
    int fieldsLeft = m_minRandomizedFields*n + rand()%(static_cast<int>(n*m_maxRandomizedFields));
    int safetyBreak = 1000;
    while (fieldsLeft)
    {
        if (safetyBreak-- == 0)
            break;

        int x = rand()%n, y = rand()%n;
        auto val = static_cast<FieldValue>(rand()%2 + 1);
        if (m_fields[x][y].get()->isModifiable() && m_judge->canPlace({x, y}, val))
        {
            m_fields[x][y] = {new Field<false>(val)};
            safetyBreak = 1000;
            --fieldsLeft;
        }
    }
}

unsigned Board::size() const noexcept
{
    return m_fields.size();
}

void Board::resize(unsigned size) noexcept
{
    if (m_fields.size() == size || size < 2 || size%2 != 0)
        return;

    m_fields.resize(size);
    for (unsigned i=0; i<size; ++i)
        m_fields[i].resize(size);

    clear();
}

FieldValue loadedCharToFieldValue(char c) noexcept
{
    if (c == '.')
        return Null;
    return static_cast<FieldValue>(c-'0'+1);
}

char fieldValueToSavedChar(FieldValue v) noexcept
{
    if (v == Null)
        return '.';
    return static_cast<char>(v)+'0'-1;
}

void Board::loadFromFile(const char *name)
{
    auto file = fopen (name , "r");
    if (file == nullptr)
    {
        randomize();
        return;
    }

    fseek(file , 0, SEEK_END);
    auto amountOfChars = ftell(file);
    rewind(file);

    auto buffer = new char[amountOfChars];
    if (buffer == nullptr)
        throw "Couldn't allocate that much memory";

    fread(buffer, 1, amountOfChars, file);

    fclose(file);

    int n = (-1 + sqrt(1 + 4*amountOfChars)) / 2; // from squared equation n(n+1) = amountOfChars
    m_fields.resize(n);
    for (int i=0; i<n; ++i)
        m_fields[i].resize(n);

    for (int y=0; y<n; ++y)
        for (int x=0; x<n; ++x)
        {
            auto fv = loadedCharToFieldValue(buffer[y*(n+1) + x]);
            if (fv != Null)
                m_fields[x][y] = {new Field<false>{fv}};
        }

    delete buffer;
}

//void Board::saveToFile(const char *name) noexcept
//{
//    int n = m_fields.size();
//    char buffer[n*(n+1)];
//    for (int y=0; y<n; ++y)
//    {
//        for (int x=0; x<n; ++x)
//            buffer[y*(n+1) + x] = fieldValueToSavedChar(m_fields[x][y].get()->getValue());
//        buffer[y*(n+1) + n] = '\n';
//    }
//    auto file = fopen(name, "w");
//    fwrite(buffer , sizeof(char), sizeof(buffer), file);
//    fclose(file);
//}
