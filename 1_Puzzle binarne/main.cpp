#include <time.h>
#include <stdlib.h>

#include "board.h"
#include "consoleprinter.h"
#include "judge.h"
#include "conio2/conio2.h"
#include "stl/vector.h"

int main()
{
    Position cursorPos;
    KeyInfo latestKeyInfo;
    Board board;
    ConsolePrinter printer{&cursorPos, &latestKeyInfo};
    Judge judge{&board};
    board.setPtrToJudge(&judge);
    Vector <char *> tips;
    bool markAssholeFields = false;
    bool showRule2Hints = false;
    bool automode = false;
    bool automodeWarning = false;

    srand(time(nullptr));

#ifndef __cplusplus
    Conio2_Init();
#endif
    settitle("Piotr Juszczyk 171841");
    do {
        printer.clear();
        printer.printMenu();
        printer.printBoard(board);
        printer.printCursor();
        printer.printTips(tips);
        if (markAssholeFields)
            printer.markAssholeFields(board, judge);
        if (showRule2Hints)
            printer.printHVLabels(board);
        if (automode)
            printer.printAutomodeInfo();
        if (automode && automodeWarning)
            printer.printAutomodeWarning();

        tips.clear();
        markAssholeFields = 0;
        showRule2Hints = 0;

        latestKeyInfo.special = 0;
        latestKeyInfo.key = getch();
        if (latestKeyInfo.key == 0)
        {
            latestKeyInfo.special = 1;
            latestKeyInfo.key = getch();
            if(latestKeyInfo.key == 0x48)
                printer.moveCursor(0,-1,board.size());
            else if(latestKeyInfo.key == 0x50)
                printer.moveCursor(0,1,board.size());
            else if(latestKeyInfo.key == 0x4b)
                printer.moveCursor(-1,0,board.size());
            else if(latestKeyInfo.key == 0x4d)
                printer.moveCursor(1,0,board.size());
        }
        else if (latestKeyInfo.key == '0')
        {
            if (judge.canPlace(cursorPos, Zero))
            {
                board.accessField(cursorPos.x, cursorPos.y)->setValue(Zero);
                automodeWarning = !judge.canCompleteTheGame();
            }
        }
        else if (latestKeyInfo.key == '1')
        {
            if (judge.canPlace(cursorPos, One))
            {
                board.accessField(cursorPos.x, cursorPos.y)->setValue(One);
                automodeWarning = !judge.canCompleteTheGame();
            }
        }
        else if (latestKeyInfo.key == 'n')
            board.reset();
        else if (latestKeyInfo.key == 'o')
            board.randomize();
        else if (latestKeyInfo.key == 'p')
            tips = judge.getTips(cursorPos);
        else if (latestKeyInfo.key == 'r')
        {
            printer.printSizePrompt(&board);
            printer.resetCursor();
        }
        else if (latestKeyInfo.key == 'k')
            markAssholeFields = true;
        else if (latestKeyInfo.key == 'd')
            showRule2Hints = true;
        else if (latestKeyInfo.key == 'a')
        {
            automode = !automode;
            if (automode)
                automodeWarning = !judge.canCompleteTheGame();
        }
        else
        {
			// TODO missing actions
        }
    } while (latestKeyInfo.key != 0x1b);
    return 0;
}
