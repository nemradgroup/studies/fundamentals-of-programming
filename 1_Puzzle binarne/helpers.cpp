#include "helpers.h"

#include <math.h>

char *intToString(int n) noexcept
{
    if (!n)
    {
        auto r = new char[2];
        r[0] = '0';
        r[1] = '\0';
        return r;
    }

    const unsigned maxDigits = 10;

    char c[maxDigits];
    int pos = maxDigits-1;
    while (n)
    {
        c[pos--] = n%10 + '0';
        n/=10;
    }
    ++pos;
    char *r = new char[maxDigits-pos];
    for (int i=pos; i<maxDigits; ++i)
        r[i-pos] = c[i];
    r[maxDigits-pos] = 0;

    return r;
}

int stringToInt(const char *s, unsigned length) noexcept
{
    int r=0;
    for (int i=0; i<length; ++i)
        r += (s[i]-'0')*pow(10, length-i-1);
    return r;
}
