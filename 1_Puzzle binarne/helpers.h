#pragma once

#ifndef min
template <class T1, class T2>
T1 min(const T1 &v1, const T2 &v2) noexcept
{
    return v1 < v2 ? v1 : v2;
}
#endif // !min

#ifndef max
template <class T1, class T2>
T1 max(const T1 &v1, const T2 &v2) noexcept
{
    return v1 > v2 ? v1 : v2;
}
#endif // !max

char *intToString(int n) noexcept;

int stringToInt(const char *s, unsigned length) noexcept;

struct Position
{
    int x = 0, y = 0;
};

struct KeyInfo
{
    char key = 0;
    bool special = 0;
};
