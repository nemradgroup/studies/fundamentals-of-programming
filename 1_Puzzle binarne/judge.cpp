#include "judge.h"

#include <string.h>

#pragma warning(disable:4996)

Judge::Judge(Board *board) noexcept
    : m_board(board) {}

void Judge::temporarilyPlace(const Position &pos, FieldValue value) noexcept
{
    if (m_tempPos.x != -1)
        revertPlacement();

    m_prevVal = m_board->getField(pos.x, pos.y)->getValue();

    m_board->accessField(pos.x, pos.y)->setValue(value);

    m_tempPos = pos;
    m_tempVal = value;
}

void Judge::revertPlacement() noexcept
{
    if (m_tempPos.x == -1)
        return;

    m_board->accessField(m_tempPos.x, m_tempPos.y)->setValue(m_prevVal);

    m_tempPos = Position{-1,-1};
}

bool Judge::checkRule1(const Position &pos, Vector <char *> *errorRedirection) const noexcept
{
    int n = m_board->size();
    int x = pos.x, y = pos.y;
    auto get = [this](int x, int y){return m_board->getField(x, y)->getValue();};
    FieldValue val = get(pos.x, pos.y);

    auto generateErrorReport = [](int w0, int wk, bool x)
    {
        char *err = new char[100];
        strcpy(err, "Zasada 1.: Powtorzenia od ");
        if (x)
            strcat(err, "x=");
        else
            strcat(err, "y=");
        auto num = intToString(w0);
        strcat(err, num);
        delete[] num;
        strcat(err, " do ");
        if (x)
            strcat(err, "x=");
        else
            strcat(err, "y=");
        num = intToString(wk);
        strcat(err, num);
        delete[] num;

        return err;
    };

    bool ok=1;

    if (x-2 >= 0)
        if (get(x-2, y) == val && get(x-1, y) == val && get(x, y) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(x-2, x, true));
                ok=0;
            }
        }
    if (x-1 >= 0 && x+1 < n)
        if (get(x-1, y) == val && get(x, y) == val && get(x+1, y) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(x-1, x+1, true));
                ok=0;
            }
        }
    if (x+2 < n)
        if (get(x, y) == val && get(x+1, y) == val && get(x+2, y) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(x, x+2, true));
                ok=0;
            }
        }
    if (y-2 >= 0)
        if (get(x, y-2) == val && get(x, y-1) == val && get(x, y) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(y-2, y, false));
                ok=0;
            }
        }
    if (y-1 >= 0 && y+1 < n)
        if (get(x, y-1) == val && get(x, y) == val && get(x, y+1) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(y-1, y+1, false));
                ok=0;
            }
        }
    if (y+2 < n)
        if (get(x, y) == val && get(x, y+1) == val && get(x, y+2) == val)
        {
            if (errorRedirection == nullptr)
                return false;
            else
            {
                errorRedirection->push_back(generateErrorReport(y, y+2, false));
                ok=0;
            }
        }

    return ok;
}

bool Judge::checkRule2(const Position &pos, Vector <char *> *errorRedirection) const noexcept
{
    int cnt0 = 0, cnt1 = 0;
    int n=m_board->size();
    auto get = [this](int x, int y){return m_board->getField(x, y)->getValue();};

    auto generateErrorReport = [](int w, bool zero, bool row)
    {
        char *err = new char[100];
        strcpy(err, "Zasada 2.: Za duzo razy w ");
        if (row)
            strcat(err, "wierszu ");
        else
            strcat(err, "kolumnie ");
        auto num = intToString(w);
        strcat(err, num);
        delete[] num;
        strcat(err, " wystepuje ");
        if (zero)
            strcat(err, "zero");
        else
            strcat(err, "jeden");

        return err;
    };

    bool ok=1;

    for (int x=0; x<m_board->size(); ++x)
    {
        auto val = get(x, pos.y);
        if (val != Null)
        {
            if (val == Zero)
            {
                if (++cnt0 > n/2)
                {
                    if (errorRedirection == nullptr)
                        return false;
                    else
                    {
                        errorRedirection->push_back(generateErrorReport(pos.y, true, true));
                        ok=0;
                        break;
                    }
                }
            }
            else
                if (++cnt1 > n/2)
                {
                    if (errorRedirection == nullptr)
                        return false;
                    else
                    {
                        errorRedirection->push_back(generateErrorReport(pos.y, false, true));
                        ok=0;
                        break;
                    }
                }
        }
    }
    cnt0 = 0, cnt1 = 0;
    for (int y=0; y<m_board->size(); ++y)
    {
        auto val = get(pos.x, y);
        if (val != Null)
        {
            if (val == Zero)
            {
                if (++cnt0 > n/2)
                {
                    if (errorRedirection == nullptr)
                        return false;
                    else
                    {
                        errorRedirection->push_back(generateErrorReport(pos.x, true, false));
                        ok=0;
                        break;
                    }
                }
            }
            else
                if (++cnt1 > n/2)
                {
                    if (errorRedirection == nullptr)
                        return false;
                    else
                    {
                        errorRedirection->push_back(generateErrorReport(pos.y, false, false));
                        ok=0;
                        break;
                    }
                }
        }
    }

    return ok;
}

bool Judge::checkRule3(const Position &pos, Vector <char *> *errorRedirection) const noexcept
{
    int n=m_board->size();
    auto get = [this](int x, int y){return m_board->getField(x, y)->getValue();};

    auto generateErrorReport = [](int w1, int w2, bool row)
    {
        char *err = new char[100];
        strcpy(err, "Zasada 3.: ");
        if (row)
            strcat(err, "Wiersz ");
        else
            strcat(err, "Kolumna ");
        auto num = intToString(w1);
        strcat(err, num);
        delete[] num;
        strcat(err, " jest taki sam/taka sama jak ");
        num = intToString(w2);
        strcat(err, num);
        delete[] num;

        return err;
    };

    bool ok=1;

    bool fullRow = 1;
    for (int x=0; x<n; ++x)
        if (get(x, pos.y) == Null)
        {
            fullRow = 0;
            break;
        }
    if (fullRow)
    {
        for (int y=0; y<n; ++y)
        {
            if (y == pos.y)
                continue;

            bool equal=1;

            for (int x=0; x<n; ++x)
                if (get(x, y) != get(x, pos.y))
                {
                    equal=0;
                    break;
                }

            if (equal)
            {
                if (errorRedirection == nullptr)
                    return false;
                else
                {
                    errorRedirection->push_back(generateErrorReport(pos.y, y, true));
                    ok=0;
                }
            }
        }

    }

    bool fullColumn = 1;
    for (int y=0; y<n; ++y)
        if (get(pos.x, y) == Null)
        {
            fullColumn = 0;
            break;
        }
    if (fullColumn)
    {
        for (int x=0; x<n; ++x)
        {
            if (x == pos.x)
                continue;

            bool equal=1;

            for (int y=0; y<n; ++y)
                if (get(x, y) != get(pos.x, y))
                {
                    equal=0;
                    break;
                }

            if (equal)
            {
                if (errorRedirection == nullptr)
                    return false;
                else
                {
                    errorRedirection->push_back(generateErrorReport(pos.x, x, false));
                    ok=0;
                }
            }
        }
    }

    return ok;
}

bool Judge::canPlace(const Position &pos, FieldValue value) noexcept
{
    temporarilyPlace(pos,value);
    bool r = checkRule1(pos) && checkRule2(pos) && checkRule3(pos);
    revertPlacement();
    return r;
}

Vector<char *> Judge::getTips(const Position &pos) noexcept
{
    Vector <char *> r;
    if (!m_board->getField(pos.x, pos.y)->isModifiable())
    {
        auto c = new char[50];
        strcpy(c, "Pole nie jest modyfikowalne");
        r.push_back(c);
        return r;
    }

    Vector <char *> errors[2];

    temporarilyPlace(pos, Zero);
    checkRule1(pos, errors);
    checkRule2(pos, errors);
    checkRule3(pos, errors);
    revertPlacement();

    if (errors[0].isEmpty())
    {
        auto c = new char[50];
        strcpy(c, "Mozna wpisac 0");
        r.push_back(c);
    }

    temporarilyPlace(pos, One);
    checkRule1(pos, errors+1);
    checkRule2(pos, errors+1);
    checkRule3(pos, errors+1);
    revertPlacement();

    if (errors[1].isEmpty())
    {
        auto c = new char[50];
        strcpy(c, "Mozna wpisac 1");
        r.push_back(c);
    }

    r+=errors[0];
    r+=errors[1];

    return r;
}

bool Judge::canCompleteTheGame() noexcept
{
    for (int i=0; i<m_board->size(); ++i)
        for (int j=0; j<m_board->size(); ++j)
            if (m_board->getField(i,j)->getValue() == Null && !(canPlace({i,j}, Zero) || canPlace({i,j}, One)))
                return false;
    return true;
}
