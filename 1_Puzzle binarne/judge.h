#pragma once

#include "helpers.h"
#include "board.h"
#include "stl/vector.h"

class Judge
{
public:
    Judge(Board *board) noexcept;

    void temporarilyPlace(const Position &pos, FieldValue value) noexcept;
    void revertPlacement() noexcept;

    bool checkRule1(const Position &pos, Vector <char *> *errorRedirection = nullptr) const noexcept;
    bool checkRule2(const Position &pos, Vector <char *> *errorRedirection = nullptr) const noexcept;
    bool checkRule3(const Position &pos, Vector <char *> *errorRedirection = nullptr) const noexcept;

    bool canPlace(const Position &pos, FieldValue value) noexcept;

    Vector <char *> getTips(const Position &pos) noexcept;

    bool canCompleteTheGame() noexcept;

private:
    Board *m_board;

    Position m_tempPos = {-1,-1};
    FieldValue m_tempVal;
    FieldValue m_prevVal;
};
