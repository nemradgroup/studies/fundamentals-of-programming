#include "consoleprinter.h"

#include <stdio.h>

#include "conio2/conio2.h"
#include "board.h"
#include "judge.h"

#pragma warning(disable:4996)

const int colorOfModifiable = LIGHTGRAY;
const int colorOfUnmodifiable = LIGHTGREEN;
const int bgColorOfNiceField = BLACK;
const int bgColorOfAssholeField = RED;

ConsolePrinter::ConsolePrinter(Position *posOfCursor, KeyInfo *keyInfo) noexcept
    : m_posOfCursor(posOfCursor), m_keyInfo(keyInfo) {}

void ConsolePrinter::clear() noexcept
{
    textbackground(bgColorOfNiceField);
    clrscr();
    textcolor(colorOfModifiable);
}

void ConsolePrinter::printMenu() noexcept
{
    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 0);
    cputs("Piotr Juszczyk 171841");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 1);
    cputs("=====================");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 2);
    cputs("Podpunkty abcdefghijkl");


    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 4);
    cputs("Legenda:");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 5);
    cputs("esc = wyjscie");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 6);
    cputs("strzalki = poruszanie");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 7);
    cputs("01 = wpisanie cyfry");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 8);
    cputs("n = nowa gra");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 9);
    cputs("o = losuj");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 10);
    cputs("p = podpowiedz");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 11);
    cputs("r = zmiana rozmiaru");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 12);
    cputs("k = sprawdzenie niefajnych pol");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 13);
    cputs("d = pokaz podpowiedzi dla zasady 2");

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 13);
    cputs("a = tryb auto");

    char txt[32];
    if (m_keyInfo->special)
        sprintf(txt, "kod klawisza: 0x00 0x%02x", m_keyInfo->key);
    else
        sprintf(txt, "kod klawisza: 0x%02x", m_keyInfo->key);
    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 14);
    cputs(txt);

    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 15);
    cputs("X: ");
    auto x = intToString(m_posOfCursor->x);
    cputs(x);
    delete[] x;
    cputs(" Y: ");
    auto y = intToString(m_posOfCursor->y);
    cputs(y);
    delete[] y;
}

void ConsolePrinter::printBoard(const Board &board) noexcept
{
    auto fvToChar = [](FieldValue val)->char
    {
        switch (val) {
        case Null:
            return ' ';
        case Zero:
            return '0';
        default:
            return '1';
        }
    };

    const char border = '#';

    gotoxy(m_posOfBoard.x, m_posOfBoard.y);
    for (int i=0; i<board.size() + 2; ++i)
        putch(border);
    for (int y=0; y<board.size(); ++y)
    {
        gotoxy(m_posOfBoard.x, m_posOfBoard.y + 1 + y);
        putch(border);
        for (int x=0; x<board.size(); ++x)
        {
            if (board.getField(x,y)->isModifiable())
                textcolor(colorOfUnmodifiable);
            else
                textcolor(colorOfModifiable);

            putch(fvToChar(board.getField(x,y)->getValue()));
        }
        textcolor(colorOfModifiable);
        putch(border);
    }
    gotoxy(m_posOfBoard.x, m_posOfBoard.y + 1 + board.size());
    for (int i=0; i<board.size() + 2; ++i)
        putch(border);
}

void ConsolePrinter::printCursor() noexcept
{
    gotoxy(m_posOfBoard.x+1+m_posOfCursor->x, m_posOfBoard.y+1+m_posOfCursor->y);
}

void ConsolePrinter::printTips(const Vector<char *> &tips) noexcept
{
    int y=m_posOfMenu.y + 18;
    for (int i=0;i<tips.size();++i)
    {
        gotoxy(m_posOfMenu.x, y++);
        cputs(tips[i]);
    }
    gotoxy(m_posOfBoard.x+1+m_posOfCursor->x, m_posOfBoard.y+1+m_posOfCursor->y);
}

void ConsolePrinter::printSizePrompt(Board *board) noexcept
{
    gotoxy(m_posOfMenu.x, m_posOfMenu.y + 20);
    cputs("Wprowadz nowy rozmiar i zatwierdz enterem: ");
    char sign;
    char num[20];
    int pos=0;
    do {
        sign = getch();
        if (sign >= '0' && sign <= '9')
        {
            putch(sign);
            num[pos++] = sign;
        }
    } while (sign != 0x0d && pos<20);
    int n = stringToInt(num, pos);
    board->resize(n);
    board->reset();
}

void ConsolePrinter::markAssholeFields(Board &board, Judge &judge) noexcept
{
    for (int y=0; y<board.size(); ++y)
        for (int x=0; x<board.size(); ++x)
            if (board.getField(x, y)->getValue() == Null &&
                    !(judge.canPlace({x,y}, Zero) || judge.canPlace({x,y}, One)))
            {
                gotoxy(m_posOfBoard.x+1+x, m_posOfBoard.y+1+y);
                textbackground(bgColorOfAssholeField);
                putch(' ');
                textbackground(bgColorOfNiceField);
            }
    gotoxy(m_posOfBoard.x+1+m_posOfCursor->x, m_posOfBoard.y+1+m_posOfCursor->y);
}

void ConsolePrinter::printHVLabels(Board &board) noexcept
{
    auto printHLabel = [](const Position &origin, int y, int amounts[2])
    {
        gotoxy(origin.x, origin.y+y);
        cputs("0 x ");
        auto s = intToString(amounts[0]);
        cputs(s);
        delete[] s;
        cputs("   1 x ");
        s = intToString(amounts[1]);
        cputs(s);
        delete[] s;
    };

    auto printVLabel = [](const Position &origin, int x, int amounts[2])
    {
        int y=origin.y;
        gotoxy(origin.x+x, y++);
        putch('0');
        gotoxy(origin.x+x, y++);
        putch('x');
        gotoxy(origin.x+x, y);
        int digits=0;
        auto s = intToString(amounts[0]);
        while (amounts[0])
        {
            amounts[0]/=10;
            ++digits;
        }
        if (!digits)
            ++digits; // zero
        for (int i=0; i<digits; ++i)
        {
            gotoxy(origin.x+x, y+i);
            putch(s[i]);
        }
        delete[] s;
        ++y;
        gotoxy(origin.x+x, y++);
        putch(' ');
        gotoxy(origin.x+x, y++);
        putch('1');
        gotoxy(origin.x+x, y++);
        putch('x');
        digits=0;
        s = intToString(amounts[1]);
        while (amounts[1])
        {
            amounts[1]/=10;
            ++digits;
        }
        if (!digits)
            ++digits; // zero
        for (int i=0; i<digits; ++i)
        {
            gotoxy(origin.x+x, y+i);
            putch(s[i]);
        }
        delete[] s;
    };

    for (int i=0; i<board.size(); ++i)
    {
        //horizontal (for rows)
        int cnt[2] = {0,0};
        for (int j=0; j<board.size(); ++j)
        {
            auto val = board.getField(j, i)->getValue();
            if (val != Null)
                ++cnt[static_cast<int>(val)-1];
        }
        printHLabel({static_cast<int>(m_posOfBoard.x+2+board.size()), m_posOfBoard.y+1}, i, cnt);

        //vertical (for columns)
        cnt[0] = 0;
        cnt[1] = 0;
        for (int j=0; j<board.size(); ++j)
        {
            auto val = board.getField(i, j)->getValue();
            if (val != Null)
                ++cnt[static_cast<int>(val)-1];
        }
        printVLabel({m_posOfBoard.x+1, static_cast<int>(m_posOfBoard.y+2+board.size())}, i, cnt);
    }
    gotoxy(m_posOfBoard.x+1+m_posOfCursor->x, m_posOfBoard.y+1+m_posOfCursor->y);
}

void ConsolePrinter::printAutomodeInfo() noexcept
{
    gotoxy(m_posOfMenu.x, m_posOfMenu.y+25);
    cputs("Tryb auto aktywny");
    gotoxy(m_posOfBoard.x +1+ m_posOfCursor->x, m_posOfBoard.y +1+ m_posOfCursor->y);
}

void ConsolePrinter::printAutomodeWarning() noexcept
{
    gotoxy(m_posOfMenu.x, m_posOfMenu.y+26);
    cputs("Nie mozna ukonczyc gry");
    gotoxy(m_posOfBoard.x +1+ m_posOfCursor->x, m_posOfBoard.y +1+ m_posOfCursor->y);
}

void ConsolePrinter::moveCursor(int x, int y, unsigned sizeOfBoard) noexcept
{
    if (m_posOfCursor->x + x >= 0 && m_posOfCursor->x + x < sizeOfBoard &&
            m_posOfCursor->y + y >= 0 && m_posOfCursor->y + y < sizeOfBoard)
    {
        *m_posOfCursor = {m_posOfCursor->x + x, m_posOfCursor->y + y};
        printCursor();
    }
}

void ConsolePrinter::resetCursor() noexcept
{
    m_posOfCursor->x=0;
    m_posOfCursor->y=0;

    gotoxy(m_posOfBoard.x, m_posOfBoard.y);
}
