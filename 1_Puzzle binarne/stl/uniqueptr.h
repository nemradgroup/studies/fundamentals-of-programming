#pragma once

template <class T>
class UniquePtrBase
{
public:
    UniquePtrBase() noexcept
        : m_object(nullptr) {}
    virtual ~UniquePtrBase() noexcept
    {
		if (m_object != nullptr)
			delete m_object;
    }

    inline operator bool() const noexcept
    {
        return m_object != nullptr;
    }

    void operator=(T *object) noexcept
    {
        if (m_object != nullptr)
            delete m_object;
        m_object = object;
    }

    T *get() noexcept
    {
        return m_object;
    }
    const T *get() const noexcept
    {
        return m_object;
    }

    void reset() noexcept
    {
        if (m_object != nullptr)
        {
            delete m_object;
            m_object = new T;
        }
    }
    T *release() noexcept
    {
        T *r = m_object;
        m_object = nullptr;
        return r;
    }

protected:
    explicit UniquePtrBase(T *object) noexcept
        : m_object(object) {}

    T *m_object;
};

template <class T>
class UniquePtr : public UniquePtrBase <T>
{
public:
    UniquePtr() = default;
    explicit UniquePtr(T *object) noexcept
        : UniquePtrBase<T>(object) {}
    explicit UniquePtr(const UniquePtr<T> &other) noexcept
        : UniquePtrBase<T>(other ? new T{*other.m_object} : nullptr) {}
    explicit UniquePtr(UniquePtr <T> &&other) noexcept
        : UniquePtrBase<T>(other.m_object)
    {
        other.m_object = nullptr;
    }
    ~UniquePtr() noexcept = default;

    inline UniquePtr<T> &operator=(T *object) noexcept
    {
        UniquePtrBase<T>::operator =(object);
        return *this;
    }
    inline UniquePtr<T> &operator=(const UniquePtr<T> &other) noexcept
    {
        if (other)
            UniquePtrBase<T>::m_object = new T{*other.m_object};
        else
            UniquePtrBase<T>::m_object = nullptr;
        return *this;
    }

    T *operator->()
    {
        if (UniquePtrBase<T>::m_object == nullptr)
            throw "Trying to access member of an unavailable resource";
        return UniquePtrBase<T>::m_object;
    }
    const T *operator->() const
    {
        if (UniquePtrBase<T>::m_object == nullptr)
            throw "Trying to access member of an unavailable resource";
        return UniquePtrBase<T>::m_object;
    }

    T *operator*() noexcept
    {
        return UniquePtrBase<T>::m_object;
    }
    const T *operator*() const noexcept
    {
        return UniquePtrBase<T>::m_object;
    }
};

template <class T>
class UniquePtr <T []> : public UniquePtrBase <T>
{
public:
    UniquePtr()
        : UniquePtrBase<T>(nullptr) {}
    explicit UniquePtr(T *object) noexcept
        : UniquePtrBase<T>(object) {}
    explicit UniquePtr(const UniquePtr<T> &other) noexcept
        : UniquePtrBase<T>(other ? new T{*other.m_object} : nullptr) {}
    explicit UniquePtr(UniquePtr <T> &&other) noexcept
        : UniquePtrBase<T>(other.m_object)
    {
        other.m_object = nullptr;
    }
    ~UniquePtr() noexcept = default;

    inline UniquePtr<T> &operator=(T *object) noexcept
    {
        UniquePtrBase<T>::operator =(object);
        return *this;
    }
    inline UniquePtr<T> &operator=(const UniquePtr<T> &other) noexcept
    {
        if (other)
            UniquePtrBase<T>::m_object = new T{*other.m_object};
        else
            UniquePtrBase<T>::m_object = nullptr;
        return *this;
    }

    T *operator[](unsigned index)
    {
        if (UniquePtrBase<T>::m_object == nullptr)
            throw "Trying to access content of an unavailable array";
        return UniquePtrBase<T>::m_object[index];
    }
    const T *operator[](unsigned index) const
    {
        if (UniquePtrBase<T>::m_object == nullptr)
            throw "Trying to access content of an unavailable array";
        return UniquePtrBase<T>::m_object[index];
    }
};
