#pragma once

#include "array.h"

template <class T>
class Vector
{
public:
    typedef T * iterator;
    typedef const T * const_iterator;

    Vector() noexcept
        : m_currentSize(0) {}
    Vector(const Vector <T> &other) noexcept
    {
        *this = other;
    }
    explicit Vector(Vector <T> &&other) noexcept
    {
        m_data = other.m_data;
        other.m_data = Array<T>{};

        m_currentSize = other.m_currentSize;
        other.m_currentSize = 0;
    }
    explicit Vector(iterator begin, iterator end) noexcept
    {
        while (begin++ != end)
            push_back(*begin);
    }
    explicit Vector(const_iterator begin, const_iterator end) noexcept
    {
        while (begin++ != end)
            push_back(*begin);
    }
    virtual ~Vector() noexcept = default;

    Vector<T> &operator=(const Vector<T> &other) noexcept
    {
        m_data = Array<T>{other.m_data};
        m_currentSize = other.m_currentSize;
        return *this;
    }

    const T &operator[](unsigned index) const noexcept
    {
        return m_data[index];
    }
    T &operator[](unsigned index) noexcept
    {
        return m_data[index];
    }

    const T &at(unsigned index) const noexcept
    {
        return m_data.at(index);
    }

    T value(unsigned index, const T &defaultValue = {}) const noexcept
    {
        return m_data.value(index, defaultValue);
    }

    T &front()
    {
        if (isEmpty())
        {
            throw "No data available!";
            static T t;
            return t;
        }
        return m_data[0];
    }
    const T &front() const
    {
        if (isEmpty())
        {
            throw "No data available!";
            static const T t;
            return t;
        }
        return m_data[0];
    }

    T &back()
    {
        if (isEmpty())
        {
            throw "No data available!";
            static T t;
            return t;
        }
        return m_data[m_currentSize - 1];
    }
    const T &back() const
    {
        if (isEmpty())
        {
            throw "No data available!";
            static const T t;
            return t;
        }
        return m_data[m_currentSize - 1];
    }

    iterator begin() noexcept
    {
        if (isEmpty())
            return iterator{nullptr};
        return iterator{&m_data[0]};
    }
    const_iterator begin() const noexcept
    {
        if (isEmpty())
            return const_iterator{nullptr};
        return const_iterator{&m_data[0]};
    }

    iterator end() noexcept
    {
        if (isEmpty())
            return iterator{nullptr};
        return iterator{&m_data[m_currentSize]};
    }
    const_iterator end() const noexcept
    {
        if (isEmpty())
            return const_iterator{nullptr};
        return const_iterator{&m_data[m_currentSize]};
    }

    void push_back(const T &element) noexcept
    {
        checkCapacity();

        m_data[m_currentSize++] = element;
    }
    Vector<T> &operator+=(const T &element) noexcept
    {
        push_back(element);
        return *this;
    }
    Vector<T> &operator+=(const Vector<T> &other) noexcept
    {
        T * content = new T[size()+other.size()];
        for (int i=0; i<size(); ++i)
            content[i]=m_data[i];
        for (int i=0; i<other.size(); ++i)
            content[i+size()]=other.m_data[i];
        m_data=Array<T>(content, size()+other.size());
        delete[] content;
        m_currentSize+=other.m_currentSize;
        return *this;
    }

    void insert(unsigned pos, const T &element) noexcept
    {
        if (pos >= m_currentSize)
            push_back(element);
        else
        {
            checkCapacity();

            for (int i=pos; i<m_currentSize; ++i)
                m_data[i+1] = m_data[i];
            ++m_currentSize;
            m_data[pos] = element;
        }
    }

    void remove(unsigned index) noexcept
    {
        if (index < m_currentSize)
        {
            for (int i=index;i<m_currentSize-1;++i)
                m_data[i] = m_data[i+1];
            --m_currentSize;
        }
    }
    void remove(unsigned index, int length) noexcept
    {
        if (index < m_currentSize)
        {
            if (length == -1 || index+length >= m_currentSize) // chop at that point
                m_currentSize = index;
            else
            {
                for (int i=index;i<m_currentSize-length;++i)
                    m_data[i] = m_data[i+1];
                m_currentSize -= length;
            }
        }
    }

    void clear() noexcept
    {
        m_data = Array<T>{};
        m_currentSize = 0;
    }

    void resize(unsigned size) noexcept
    {
        if (size == this->capacity())
            return;

        bool extend = size > this->capacity();

        int limit = extend ? m_currentSize : size;
        auto temp = m_data;
        T *newContent = new T[size];
        m_data = Array<T>{newContent, size};
        delete[] newContent;
        for (int i=0; i<limit; ++i)
            m_data[i] = temp[i];

        m_currentSize = size;
    }
    void reserve(unsigned capacity) noexcept
    {
        if (capacity == this->capacity())
            return;

        bool extend = capacity > this->capacity();

        int limit = extend ? this->capacity() : capacity;
        auto temp = m_data;
        T *newContent = new T[capacity];
        m_data = Array<T>{newContent, capacity}; // well, not really, but I don't have time for that crap
        delete[] newContent;
        for (int i=0; i<limit; ++i)
            m_data[i] = temp[i];

        if (!extend)
            m_currentSize = capacity;
    }

    unsigned size() const noexcept
    {
        return m_currentSize;
    }
    unsigned capacity() const noexcept
    {
        return m_data.size();
    }

    bool isEmpty() const noexcept
    {
        return m_data.isEmpty();
    }

private:
    void checkCapacity() noexcept
    {
        if (size() == capacity())
            reserve(capacity() != 0 ? capacity()*m_reserveIcreaseMagnitude : 2);
    }

    Array<T> m_data;
    unsigned m_currentSize;
    const float m_reserveIcreaseMagnitude = 2.0;
};
