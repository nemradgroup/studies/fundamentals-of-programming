#pragma once

enum FieldValue
{
    Null,
    Zero,
    One
};

class FieldBase
{
public:
    FieldBase() noexcept
        : m_value(FieldValue::Null) {}
    explicit FieldBase(FieldValue value) noexcept
        : m_value(value) {}
    virtual ~FieldBase() noexcept = default;

    virtual bool isModifiable() const noexcept {return false;}//should be pure virtual but then there will be problems with instantiation in UniquePtr

    inline FieldValue getValue() const noexcept
    {
        return m_value;
    }

    virtual void setValue(FieldValue) noexcept {}//that too

protected:
    FieldValue m_value;
};

template <bool modifiable>
class Field : public FieldBase
{
public:
    Field() noexcept = default;
    explicit Field(FieldValue value) noexcept
        : FieldBase(value) {}

    inline bool isModifiable() const noexcept final
    {
        return modifiable;
    }
    inline void setValue(FieldValue value) noexcept final
    {
        if (modifiable)
            m_value = value;
    }
};
