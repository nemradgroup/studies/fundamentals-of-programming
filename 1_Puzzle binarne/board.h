#pragma once

#include "stl/vector.h"
#include "stl/uniqueptr.h"
#include "field.h"

class Judge;

typedef Vector <Vector <UniquePtr <FieldBase> > > Fields;

class Board
{
public:
    Board() noexcept;

    inline void setPtrToJudge(Judge *judge) noexcept
    {
        m_judge=judge;
    }

    FieldBase *accessField(unsigned x, unsigned y) noexcept;
    const FieldBase *getField(unsigned x, unsigned y) const noexcept;
    void setField(unsigned x, unsigned y, FieldBase *field) noexcept;

    void clear() noexcept; // just clears
    void reset() noexcept; // clears and reloads default

    void randomize() noexcept;

    unsigned size() const noexcept;
    void resize(unsigned size) noexcept;

    void loadFromFile(const char *name);
//    void saveToFile(const char *name) noexcept;

private:
    Judge *m_judge;
    Fields m_fields;

    const float m_randomizationTickets = 0.5; // per unit
    const float m_minRandomizedFields = 0.55; // per unit per ticket
    const float m_maxRandomizedFields = 0.75; // per unit per ticket
};
